package com.rpl.dapurkos.dapurkos;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

/**
 * Created by Aji Guna on 29/12/2016.
 */

public class RumahActivity extends AppCompatActivity{
    private RecyclerView mBlogList;
    private DatabaseReference mDatabase;

    //firebase auth object
    private FirebaseAuth firebaseAuth;
    private StorageReference mStorageImage;
    private DatabaseReference mDatabaseUsers;
    private DatabaseReference mDatabaseLike;
    private DatabaseReference mDatabaseCurrentUsers;
    private Query mQueryCurrentUser;

    private boolean mProcessLike = false;

    //view objects
    private TextView textViewUserEmail;
    //private FloatingActionButton buttonLogout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //initializing firebase authentication object
        firebaseAuth = FirebaseAuth.getInstance();

        mDatabase = FirebaseDatabase.getInstance().getReference().child("blog_resep");
        mDatabaseUsers = FirebaseDatabase.getInstance().getReference().child("Users");
        mDatabaseLike = FirebaseDatabase.getInstance().getReference().child("Likes");

        String currentUserId = firebaseAuth.getCurrentUser().getUid();
        mDatabaseCurrentUsers = FirebaseDatabase.getInstance().getReference().child("blog_resep");


        mQueryCurrentUser = mDatabaseCurrentUsers.orderByChild("uid").equalTo(currentUserId);

        mDatabaseUsers.keepSynced(true);
        mDatabaseLike.keepSynced(true);
        mDatabase.keepSynced(true);

        mBlogList = (RecyclerView) findViewById(R.id.blog_list);
        mBlogList.setHasFixedSize(true);
        mBlogList.setLayoutManager(new LinearLayoutManager(this));


        //if the user is not logged in
        //that means current user will return null
        if(firebaseAuth.getCurrentUser() == null){
            //closing this activity
            finish();
            //starting login activity
            startActivity(new Intent(this, LoginActivity.class));
        }


        //getting current user
        FirebaseUser user = firebaseAuth.getCurrentUser();



       //buttonLogout = (FloatingActionButton) findViewById(buttonLogout);

        //adding listener to button
        //buttonLogout.setOnClickListener(this);
        //initializing firebase authentication object
        // firebaseAuth = FirebaseAuth.getInstance();
        checkUserExist();
    }

    @Override
    protected void onStart(){
        super.onStart();


        FirebaseRecyclerAdapter<Blog, BlogViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Blog, BlogViewHolder>(

                Blog.class,
                R.layout.blog_row,
                BlogViewHolder.class,
                mDatabase

        ) {
            @Override
            protected void populateViewHolder(BlogViewHolder viewHolder, Blog model, int position) {

                final String post_key = getRef(position).getKey();

                viewHolder.setJudul(model.getJudul());
                //viewHolder.setDesc(model.getDesc_resep());
                viewHolder.setImage(getApplicationContext(), model.getImage());
                viewHolder.setUsername(model.getUsername());
                viewHolder.photouser(getApplicationContext(), model.getImage());
                //viewHolder..setImageResource(post_key);

                viewHolder.setLikeBtn(post_key);
                viewHolder.nView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        //Toast.makeText(RumahActivity.this, post_key,Toast.LENGTH_LONG).show();
                        Intent singleBlogIntent = new Intent(RumahActivity.this, Lihat_resep.class);
                        singleBlogIntent.putExtra("blog_id",post_key);
                        startActivity(singleBlogIntent);
                    }
                });

                viewHolder.mLikebtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        mProcessLike = true;

                            mDatabaseLike.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {

                                    if (mProcessLike) {
                                        if (dataSnapshot.child(post_key).hasChild(firebaseAuth.getCurrentUser().getUid())) {

                                            mDatabaseLike.child(post_key).child(firebaseAuth.getCurrentUser().getUid()).removeValue();
                                            mProcessLike = false;

                                        } else {
                                            mDatabaseLike.child(post_key).child(firebaseAuth.getCurrentUser().getUid()).setValue("RandomValue");

                                            mProcessLike = false;
                                        }
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });

                    }
                });

            }
        };

        mBlogList.setAdapter(firebaseRecyclerAdapter);

    }

    private void checkUserExist() {

        final String user_id = firebaseAuth.getCurrentUser().getUid();

        mDatabaseUsers.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (!dataSnapshot.hasChild(user_id)){

                    Intent setupIntent = new Intent(RumahActivity.this, SetupActivity.class);
                    setupIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(setupIntent);

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    public static class BlogViewHolder extends RecyclerView.ViewHolder{

        View nView;

        ImageButton mLikebtn;

        DatabaseReference mDatabaseLike;
        FirebaseAuth mAuth;

        public BlogViewHolder(View itemView) {
            super(itemView);

            nView = itemView;
            mLikebtn = (ImageButton) nView.findViewById(R.id.like_btn);

            mDatabaseLike = FirebaseDatabase.getInstance().getReference().child("Likes");
            mAuth = FirebaseAuth.getInstance();
        }

        public void setLikeBtn(final String post_key){

            mDatabaseLike.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    if (dataSnapshot.child(post_key).hasChild(mAuth.getCurrentUser().getUid())){

                        mLikebtn.setImageResource(R.mipmap.ic_thumb_up_red_24dp);
                    }else {

                        mLikebtn.setImageResource(R.mipmap.ic_thumb_up_gray_24dp);

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }

        public void setJudul(String judul){

            TextView post_judul = (TextView) nView.findViewById(R.id.post_judul);
            post_judul.setText(judul);

        }

//        public void setDesc(String desc){
//
//            TextView post_desc = (TextView) nView.findViewById(R.id.post_resep);
//            post_desc.setText(desc);
//        }

        public void setUsername(String username){
            TextView post_username = (TextView) nView.findViewById(R.id.post_username);
            post_username.setText(username);

        }

        public void photouser(Context ctx, String image_user){

            ImageView imageku_user = (ImageView)nView.findViewById(R.id.photo_user);
            Picasso.with(ctx).load(image_user).into(imageku_user);

        }

        public void setImage(Context ctx, String image){
            ImageView post_image = (ImageView)nView.findViewById(R.id.post_image);
            Picasso.with(ctx).load(image).into(post_image);

        }



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId()==R.id.action_add){
            startActivity(new Intent(RumahActivity.this, Tulis_resep.class));
        }

        if (item.getItemId()==R.id.action_logout){
            firebaseAuth.signOut();
            finish();
            //starting login activity
            startActivity(new Intent(this, LoginActivity.class));

        }
        if (item.getItemId()==R.id.action_setting){
            startActivity(new Intent(this, SetupActivity.class));

        }

        return super.onOptionsItemSelected(item);
    }



    /*@Override
    public void onClick(View view) {
        //if logout is pressed
        if(view == buttonLogout){
            //logging out the user
            firebaseAuth.signOut();
            //closing activity
            finish();
            //starting login activity
            startActivity(new Intent(this, LoginActivity.class));
        }
    }*/
}
