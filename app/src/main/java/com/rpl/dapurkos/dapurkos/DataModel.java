package com.rpl.dapurkos.dapurkos;

/**
 * Created by Ibnu on 08/12/2016.
 */
public class DataModel {

    public int icon;
    public String name;

    // Constructor.
    public DataModel(int icon, String name) {

        this.icon = icon;
        this.name = name;
    }
}