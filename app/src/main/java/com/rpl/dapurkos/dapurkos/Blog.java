package com.rpl.dapurkos.dapurkos;

/**
 * Created by Aji Guna on 26/12/2016.
 */

public class Blog {

    private String judul;
    private String desc_resep;
    private String image;
    private String username;
    private String komentar;

    public Blog(){

    }

    public Blog(String judul, String desc_resep, String image, String komentar) {
        this.judul = judul;
        this.desc_resep = desc_resep;
        this.image = image;
        this.komentar = komentar;
        this.username = username;
    }

    public String getKomentar() {
        return komentar;
    }

    public void setKomentar(String komentar) {
        this.komentar = komentar;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getDesc_resep() {
        return desc_resep;
    }

    public void setDesc_resep(String desc_resep) {
        this.desc_resep = desc_resep;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
