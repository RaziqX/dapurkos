package com.rpl.dapurkos.dapurkos;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import static com.rpl.dapurkos.dapurkos.R.id.post_harga;
import static com.rpl.dapurkos.dapurkos.R.id.post_judul;


/**
 * Created by Aji Guna on 29/12/2016.
 */

public class Lihat_resep extends AppCompatActivity {

    private RecyclerView mKomenList;

    private String mPost_key = null;
    private EditText mPostComment;
    private ProgressDialog mProgress;

    private DatabaseReference mDatabase;
    private DatabaseReference mDatabaseku;
    private FirebaseUser mCurrentUser;
    private DatabaseReference mDatabaseUser;

    private ImageView mBlogSingleImage;
    private TextView mBlogSingJudul;
    private TextView mBlogSingHarga;
    private TextView mBlogSingleDesc;

    private FirebaseAuth mAuth;

    private Button mSingleRemove;
    private Button mBtnKomen;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resep);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("blog_resep");
        mDatabaseku = FirebaseDatabase.getInstance().getReference().child("Komentar");
        mAuth = FirebaseAuth.getInstance();
        mPost_key = getIntent().getExtras().getString("blog_id");

        mPostComment =(EditText)findViewById(R.id.teks_comment);
        mCurrentUser = mAuth.getCurrentUser();
        mDatabaseUser = FirebaseDatabase.getInstance().getReference().child("Users").child(mCurrentUser.getUid());
        mProgress = new ProgressDialog(this);

        mBlogSingleDesc = (TextView) findViewById(R.id.post_resep);
        mBlogSingleImage = (ImageView)findViewById(R.id.post_image);
        mBlogSingJudul = (TextView)findViewById(post_judul);
        mBlogSingHarga = (TextView)findViewById(post_harga);

        mSingleRemove = (Button)findViewById(R.id.removeBtn);
        mBtnKomen = (Button)findViewById(R.id.btn_komen);

        mKomenList = (RecyclerView) findViewById(R.id.komen_list);
        mKomenList.setHasFixedSize(true);
        mKomenList.setLayoutManager(new LinearLayoutManager(this));

        mDatabase.child(mPost_key).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String post_judul = (String) dataSnapshot.child("judul").getValue();
                String post_harga = (String) dataSnapshot.child("harga").getValue();
                String post_desc = (String) dataSnapshot.child("desc_resep").getValue();
                String post_image = (String)dataSnapshot.child("image").getValue();
                String post_uid = (String)dataSnapshot.child("uid").getValue();

                mBlogSingJudul.setText(post_judul);
                mBlogSingleDesc.setText(post_desc);
                mBlogSingHarga.setText(post_harga);

                Picasso.with(Lihat_resep.this).load(post_image).into(mBlogSingleImage);

                if (mAuth.getCurrentUser().getUid().equals(post_uid)){

                    //Toast.makeText(Lihat_resep.this, mAuth.getCurrentUser().getUid(), Toast.LENGTH_LONG).show();

                    mSingleRemove.setVisibility(View.VISIBLE);

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        //Toast.makeText(Lihat_resep.this, post_key, Toast.LENGTH_LONG).show();
        //initializing firebase authentication object
        //firebaseAuth = FirebaseAuth.getInstance();

        //mDatabase = FirebaseDatabase.getInstance().getReference().child("blog_resep");

        mSingleRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               mDatabase.child(mPost_key).removeValue();

                Intent mainIntent = new Intent(Lihat_resep.this, RumahActivity.class);
                startActivity(mainIntent);
            }
        });

        mBtnKomen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startPostingku();
            }
        });

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        return super.onOptionsItemSelected(item);
    }

    private void startPostingku() {
        mProgress.setMessage("Komentar....");
        mProgress.show();

        final String comment_val = mPostComment.getText().toString().trim();

        if (!TextUtils.isEmpty(comment_val)){

                    final DatabaseReference newPost = mDatabaseku.child(mPost_key).push();

                    mDatabaseUser.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            newPost.child("komentar").setValue(comment_val);
                            newPost.child("uid").setValue(mCurrentUser.getUid());
                            newPost.child("username").setValue(dataSnapshot.child("name").getValue()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                    if (task.isSuccessful()){

                                    }
                                }
                            });
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {



                        }
                    });
            mProgress.dismiss();
        }

    }


    @Override
    protected void onStart(){
        super.onStart();


        FirebaseRecyclerAdapter<Blog, BlogViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Blog, BlogViewHolder>(

                Blog.class,
                R.layout.blog_comment,
                BlogViewHolder.class,
                mDatabaseku.child(mPost_key)

        ) {
            @Override
            protected void populateViewHolder(BlogViewHolder viewHolder, Blog model, int position) {

                final String post_key = getRef(position).getKey();

                viewHolder.setUsername(model.getUsername());
                //viewHolder.setDesc(model.getDesc_resep());
                viewHolder.setKomen(model.getKomentar());
//                viewHolder.photouser(getApplicationContext(), model.getImage());
                //viewHolder..setImageResource(post_key);


            }
        };

        mKomenList.setAdapter(firebaseRecyclerAdapter);

    }

    public static class BlogViewHolder extends RecyclerView.ViewHolder{

        View nView;

        FirebaseAuth mAuth;

        public BlogViewHolder(View itemView) {
            super(itemView);

            nView = itemView;
            mAuth = FirebaseAuth.getInstance();
        }

        public void setUsername(String username){
            TextView post_username = (TextView) nView.findViewById(R.id.post_username);
            post_username.setText(username);

        }
        public void setKomen(String komentar){

            TextView post_komen = (TextView) nView.findViewById(R.id.post_komen);
            post_komen.setText(komentar);

        }

//        public void photouser(Context ctx, String image){
//
//            ImageView image_user = (ImageView)nView.findViewById(R.id.photo_user);
//            Picasso.with(ctx).load(image).into(image_user);
//
//        }

    }


}
